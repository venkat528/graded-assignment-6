package com.hcl.movies.factory;

	import java.sql.SQLException;
	import java.util.List;

import com.hcl.movies.pojo.Movie;

	public interface Classification {

		public List<Movie> movieType() throws SQLException;

	}



